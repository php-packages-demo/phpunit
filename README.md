# phpunit

Unit Testing framework https://phpunit.de/

* [*Fixtures, the right gestures*](https://blog.theodo.fr/2019/04/fixtures-right-gestures/) 2019  Victor Lebrun
* [*How to manage fixtures in a PHP project*](https://blog.theodo.fr/2013/08/managing-fixtures/) 2013 Benjamin Grandfond
